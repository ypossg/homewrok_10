#!/usr/bin/env bash
echo '============================================================================='
echo '  Simple Test Script for sqlite3 command'
echo '============================================================================='
#  This script executes basic commands to test if the sqlite3 command works
#  correctly. Feel free to PR any additional tests you would like to add.
#
#  Note: This script must be POSIX compatible since the image does not contain
#        shells such as bash or zsh. Use `shellcheck` to check for any issues.

# Check if sqlite3 is installed
which sqlite3 >/dev/null || {
  echo 'sqlite3 command not found.' >&2
  exit 1
}

# Print the current version of sqlite3
echo 'SQLite3 version:' "$(sqlite3 --version)"

# Path to the test database
name_file_db='test.db'
path_dir_tmp=$(dirname "$(mktemp -u)")
path_file_db="${path_dir_tmp}/${name_file_db}"

# Remove the existing test database
rm -f "$path_file_db"